<?php
if($_POST['crypt'] === "ЗашифроватьИзФайла" || $_POST['crypt'] === "РасшифроватьИзФайла") {
    $_POST['inputMessage'] = file_get_contents('../inputMessage.txt');
}

if(null !==([$_POST['inputMessage']])) {
    $inputMessage = mb_strtoupper($_POST['inputMessage']);
}

if(null !==($_POST['key'])) {
    $key = mb_strtoupper($_POST['key']);
}

function decryptMessage($inputMessage, $key, $action) {
    if($_POST['lang']=='lang_en') $alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T' ,'U', 'V', 'W', 'X', 'Y', 'Z']; //26
    if($_POST['lang']=='lang_ru') $alphabet = ['А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Э', 'Ю', 'Я']; //29
    $alphabetLenght = count($alphabet);
    $msgLenght = mb_strlen($inputMessage);
    $inputMessageArray = mb_str_split($inputMessage);
    $keyArray = mb_str_split($key);
    $keyLenght = mb_strlen($key);
    $result = "";

    $keyExtended = [];
    $temp = 0;

    for ($j = 0; $j < $msgLenght; $j++) {
        $keyExtended[$j] = $keyArray[$temp];
        $temp++;

        if($temp === $keyLenght) { //Если дошли до конца нерасширенного ключа, то начинаем сначала
            $temp = 0;
        }
    }

    $temp = $symAlphabet = $symKey = 0;

    foreach ($inputMessageArray as $symbolAlphabet) {
        $decryptedSymbol = "";
        for ($i = 0; $i < $alphabetLenght; $i++) {
            if($symbolAlphabet == $alphabet[$i]) { //Поиск символа в алфавите
                $symAlphabet = $i; //Запомнить индекс символа-текста
                for ($j = 0; $j < $alphabetLenght; $j++) {
                    if ($keyExtended[$temp] == $alphabet[$j]) $symKey = $j; //Запомнить индекс символа-ключа
                }
                $temp++; //После того, как нашли индекс символа-ключа в алфавите, переходим к след. символу-ключу
                if($action === "Зашифровать" || $action === "ЗашифроватьИзФайла") $d = ($symAlphabet + $symKey) % $alphabetLenght; //Формула для нахождения зашифрованного символа
                else if($action === "Расшифровать" || $action === "РасшифроватьИзФайла") $d = ($symAlphabet - $symKey + $alphabetLenght) % $alphabetLenght;
                else exit("Передано неизвестное действие");
                $decryptedSymbol = $alphabet[$d];
            }
        }
        $result .= $decryptedSymbol;
    }
    file_put_contents('result.txt', $result); //Занести результат в файл
    return array($result, implode('', $alphabet), $inputMessage, $key);
}
    //Запись необходимых переменных в ассоциативный массив сессий
    session_start();
    $_SESSION['inputMessage'] = decryptMessage($inputMessage, $key, $_POST['crypt'])[2];
    $_SESSION['key'] = decryptMessage($inputMessage, $key, $_POST['crypt'])[3];
    $_SESSION['alphabet'] = decryptMessage($inputMessage, $key, $_POST['crypt'])[1];
    $_SESSION['result'] = decryptMessage($inputMessage, $key, $_POST['crypt'])[0];
    header('location: http://localhost/ib/index.php');
?>
<?php
    session_start();
    echo "<div class='content'>";
    echo "Алфавит: " . $_SESSION['alphabet'] . "<br>";
    echo "Введенное сообщение: " . $_SESSION['inputMessage'] . "<br>";
    echo "Ключ: " . $_SESSION['key'] . "<br>";
    echo "Результат: " . $_SESSION['result'] . "<br>";
    echo "</div>";
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="windows-1251">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css">
    <title>Шифр Виженера</title>
</head>
<body>
    <div class="content">
        <form id='form' name='secretInfo' method='post' action='handlers/crypt.php'>
            <input type="radio" name="lang" value="lang_en">Английский алфавит<br>
            <input type="radio" name="lang" value="lang_ru">Русский алфавит<br>
            <textarea name='inputMessage' cols='30' rows='5' placeholder="Введите сообщение"></textarea>
            <br>
            <label for='key'>Ключ</label>
            <input type='text' name='key' required>

            <br>
            <input type='submit' name='crypt' value='Зашифровать'>
            <input type='submit' name='crypt' value='ЗашифроватьИзФайла'> <br>
            <input type='submit' name='crypt' value='Расшифровать'>
            <input type='submit' name='crypt' value='РасшифроватьИзФайла'> <br>
            Пример сообщения:       ATTACKATDAWN <br>
            Пример ключа:                 LEMON <br>
            Зашифрованный текст:  LXFOPVEFRNHR <br> <br>
        </form>

</body>
</html>